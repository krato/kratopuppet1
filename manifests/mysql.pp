class kratopuppet1::mysql {
	$packages = "mysql-server mysql-client "
	packages_install{ $packages: packages => $packages }
	-> service { 'mysql': ensure => running }
}
