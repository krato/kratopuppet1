Exec { path => [
	"/usr/local/sbin",
	"/usr/local/bin",
	"/usr/sbin",
	"/usr/bin",
	"/sbin"
] }

class kratopuppet1 {
	cron { "apt-update":
		command => "/usr/bin/apt-get update",
		user    => root,
		hour    => 22,
		minute  => 0;
	}

	exec { 'apt-update':
		command => "/usr/bin/apt-get update",
        	#refreshonly => true,
		onlyif => "/bin/bash -c 'exit $(( $(( $(date +%s) - $(stat -c %Y /var/lib/apt/lists/$( ls /var/lib/apt/lists/ -tr1|tail -1 )) )) <= 604800 ))'"
	}
	#command => "add-apt-repository -y ppa:x2go/stable && add-apt-repository -y ppa:zfs-native/stable && /usr/bin/apt-get update"

	define packages_install( $packages ) {
	        $packagesArray = split($packages, " ")
	        package { $packagesArray:
	                ensure => latest,
	                require => Exec["apt-update"]
	        }
	}

	$packages = "screen mc gparted git vim links elinks htop iotop cryptsetup puppet vim-puppet openssh-server libc6-dev tree cifs-utils sshfs smbclient openvpn software-properties-common python-software-properties clamav clamav-docs kdiff3 krename p7zip zip partclone "
	packages_install{ 'base_packages': packages => $packages }
}
