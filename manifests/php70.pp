# Please, add manually  the Dotdeb repository: https://www.dotdeb.org/instructions/
# Don't forget to: apt-get update
# Sorry for inconvenience.

class kratopuppet1::php70 {
	$packages = "php7.0 php7.0-cli php7.0-mysql php7.0-fpm php-gd "
	packages_install{ $packages: packages => $packages }
	-> service { 'php7.0-fpm': ensure => running }
}
