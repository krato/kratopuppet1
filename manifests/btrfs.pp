class kratopuppet1::btrfs ($apt_snapshots = false) {
	$packages = "btrfs-tools mdadm "
	packages_install{ $packages: packages => $packages }

	if $apt_snapshots {
		$packages2 = "apt-btrfs-snapshot "
		packages_install{ $packages2: packages => $packages2 }
	}
}
