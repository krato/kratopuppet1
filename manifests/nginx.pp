class kratopuppet1::nginx {
	$packages = "nginx "
	packages_install{ $packages: packages => $packages }
	-> service { 'nginx': ensure => running }

	file { "/etc/nginx/include":
		ensure => directory,
	}

	file { "/etc/nginx/websites":
		ensure => directory,
	}

	file { "/etc/nginx/include/default":
		ensure => link,
		target => '/etc/puppet/modules/kratopuppet1/files/etc/nginx/include/default',
		notify => Service['nginx'],
	}

	file { "/etc/nginx/sites-enabled/default":
		ensure => link,
		target => '/etc/puppet/modules/kratopuppet1/files/etc/nginx/sites-enabled/default',
		notify => Service['nginx'],
	}
}
