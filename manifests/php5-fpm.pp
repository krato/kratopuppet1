# Please, add manually  the Dotdeb repository: https://www.dotdeb.org/instructions/
# Don't forget to: apt-get update
# Sorry for inconvenience.

class kratopuppet1::php5-fpm {
	$packages = "php5-fpm php5-mysql "
	packages_install{ $packages: packages => $packages }
	-> service { 'php5-fpm': ensure => running }
}
