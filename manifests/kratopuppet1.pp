Exec { path => [
	"/usr/local/sbin",
	"/usr/local/bin",
	"/usr/sbin",
	"/usr/bin",
	"/sbin"
] }

class base {
	cron { "apt-update":
		command => "/usr/bin/apt-get update",
		user    => root,
		hour    => 22,
		minute  => 0;
	}

	exec { 'apt-update':
		command => "/usr/bin/apt-get update",
        	#refreshonly => true,
		onlyif => "/bin/bash -c 'exit $(( $(( $(date +%s) - $(stat -c %Y /var/lib/apt/lists/$( ls /var/lib/apt/lists/ -tr1|tail -1 )) )) <= 604800 ))'"
	}
	#command => "add-apt-repository -y ppa:x2go/stable && add-apt-repository -y ppa:zfs-native/stable && /usr/bin/apt-get update"
}

define packages_install( $packages ) {
        $packagesArray = split($packages, " ")
        package { $packagesArray:
                ensure => latest,
                require => Exec["apt-update"]
        }
}

class base_packages {
	$packages = "screen mc gparted git vim links htop iotop cryptsetup puppet vim-puppet openssh-server libc6-dev "
	packages_install{ 'base_packages': packages => $packages }
}

class zfs_packages {
	$packages = "ubuntu-zfs zfsutils zfs-doc zfs-auto-snapshot mdadm "
	packages_install{ 'base_packages': packages => $packages }
}

class btrfs_packages {
	$packages = "btrfs-tools apt-btrfs-snapshot mdadm "
	packages_install{ 'base_packages': packages => $packages }
}

class experimental_packages {
	$packages = "x2goclient x2goserver x2goserver-extensions "
	packages_install{ 'base_packages': packages => $packages }
}

class desktop_packages {
	$packages = "chromium-browser chromium-browser-l10n firefox firefox-locale-en opera openarena krusader virtualbox-nonfree vagrant skype kdiff3 krename vlc kdenlive easystroke wine browser-plugin-vlc libc6-dev adobe-flashplugin telnet putty collectd kcollectd "
	packages_install{ 'base_packages': packages => $packages }
}

class { base: }
class { base_packages: }

